The following is the list of books in my personal library.  A lot of these I've
only partially read.

## Literature

- Falk, Dan - *The Science of Shakespeare*
- Shakespeare, William - *Hamlet*
- Shakespeare, William - *Romeo and Juliet*
- Thomas Mautner - *The Penguin Dictionary of Philosophy*
- Tillyard, E. M. W. - *The Elizabethan World Picture*

## Mathematics

- Burton, David M. - *Elementary Number Theory*
- Hoffstein, Jeffrey and Pipher, Jill and Silverman, Joseph H. - *An Introduction to Mathematical Cryptography*
- Paolo, Aluffi - *Algebra: Chapter 0*

## Programming

- Barski, Conrad – *Land of Lisp*
- Fischer, Charles N. and Cytron, Ron K. and LeBlanc, Richard J. Jr. - *Crafting a Compiler*
- Kernighan, Brian W. and Ritchie, Dennis M. – *The C Programming Language*
- Lipovača, Miran - *Learn You a Haskell for Great Good!*
- Stevens, Richard W. - *UNIX Network Programming*
- Raymond, Eric S. - *The Cathedral and the Bazaar*
