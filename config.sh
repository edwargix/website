#!/bin/sh

# Usage: config.sh page base [menuitem ...]

PAGE=$1
NAME="ctx->$(echo ${PAGE%.*} | sed 's|/|->|g')"
shift
BASE=$1
shift
MENUPAGES="$@"

for P in $MENUPAGES; do
    I=$(basename $P .html)
    test $P == $PAGE && CUR=' class="active"' || CUR=''
    test -n "$MENU" && SEP="NEWL" # the newline hack
    MENU="$MENU$SEP<li$CUR><a href=\"$BASE$P\">$I</a></li>"
done

sed "s|\${PAGE}|$PAGE|" | \
sed "s|\${NAME}|$NAME|" | \
sed "s|\${BASE}|$BASE|" | \
sed "s|\${MENU}|$MENU|" | \
sed "s|$SEP|"'\
|g'
