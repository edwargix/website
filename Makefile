NAME = divzero
TARG = /var/www/$(NAME)
BASE = ""
#BASE = "$(PWD)/" # uncomment for devel only

MENUPAGES = home.html projects.html blog.html library.html contact.html
PAGES = $(MENUPAGES)
EXTRA = css snoopy.png favicon.ico index.html \
        lectures/resources

all: $(PAGES)
	ln -sf home.html index.html

package: all
	tar -cvzf $(NAME).tar.gz $(PAGES) $(EXTRA)

install: package
	mkdir -p $(TARG)
	tar -xzf $(NAME).tar.gz -C $(TARG)

clean:
	rm -f $(PAGES) $(NAME).tar.gz index.html

.SUFFIXES: .dhtml .shtml .md .html

DHEADER = header.dhtml
DFOOTER = footer.dhtml
SHEADER = header.shtml
SFOOTER = footer.shtml

.shtml.html: $(DHEADER) $(DFOOTER)
	cat $(DHEADER) | ./config.sh $@ $(BASE) $(MENUPAGES) > $(SHEADER)
	cat $(DFOOTER) | ./config.sh $@ $(BASE) $(MENUPAGES) > $(SFOOTER)
	cat $(SHEADER) $< $(SFOOTER) > $@
	rm $(SHEADER) $(SFOOTER)

.md.html: $(DHEADER) $(DFOOTER)
	cat $(DHEADER) | ./config.sh $@ $(BASE) $(MENUPAGES) > $(SHEADER)
	cat $(DFOOTER) | ./config.sh $@ $(BASE) $(MENUPAGES) > $(SFOOTER)
	markdown $< | cat $(SHEADER) - $(SFOOTER) > $@
	rm $(SHEADER) $(SFOOTER)
